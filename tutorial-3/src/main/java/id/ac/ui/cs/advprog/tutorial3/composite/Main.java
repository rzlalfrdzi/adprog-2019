package id.ac.ui.cs.advprog.tutorial3.composite;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();
        Employees brook = new Ceo("Brook", 230000);
        company.addEmployee(brook);

        Employees drook = new FrontendProgrammer("Drook", 200000);
        company.addEmployee(drook);

        System.out.println(company.getNetSalaries());
    }
}
