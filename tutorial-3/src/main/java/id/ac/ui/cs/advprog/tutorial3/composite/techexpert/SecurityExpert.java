package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary){
        this.role = "Network Expert";
        if (salary < 70000.00){
            throw new IllegalArgumentException();
        }
        this.salary = salary;
        this.name = name;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
