package id.ac.ui.cs.advprog.tutorial3.decorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class Main {
    public static void main(String[] args) {

        Food BurrgerMAC = BreadProducer.THICK_BUN.createBreadToBeFilled();
        BurrgerMAC = FillingDecorator.TOMATO.addFillingToBread(BurrgerMAC);
        BurrgerMAC = FillingDecorator.CUCUMBER.addFillingToBread(BurrgerMAC);
        BurrgerMAC = FillingDecorator.LETTUCE.addFillingToBread(BurrgerMAC);
        BurrgerMAC = FillingDecorator.BEEF_MEAT.addFillingToBread(BurrgerMAC);
        BurrgerMAC = FillingDecorator.CHEESE.addFillingToBread(BurrgerMAC);

        System.out.println(BurrgerMAC.getDescription());
        System.out.println(BurrgerMAC.cost());
    }
}
